*** Settings ***
Library			Selenium2Library
Library			Screenshot
Library			HttpRequestLibrary

*** Variables ***
${CVV.TOOLTIP}		//*[@class="tooltipcss"]
${CVV.TOOLTIP.MSG}	//*[text()="Please look at the 3 digits at the back of your card"]

${CUST.NAME.ERROR.MSG}			//*[contains(text(),"Card Holder required.")]
${CUST.CARD.NUMBER.ERROR.MSG}	//*[contains(text(),"Invalid Card Number.")]
${CUST.CARD.EXPIRY.DATE.MSG}	//*[contains(text(),"Invalid Expiry Date.")]
${CUST.CARD.CVV2.ERROR.MSG}		//*[contains(text(),"Invalid Security Code.")]

${CUST.NAME.TXTFLD}				//*[@id="holderName"]
${CUST.CARD.NUMBER.TXTFLD}		//*[@id="cc"]
${CUST.CARD.EXPIRY.DATE.TXTFLD}	//*[@id="ccexpiry"]
${CUST.CARD.CVV2.TXTFLD}		//*[@id="cvv2"]
${SUBMIT.BTN}					//*[@id="btnSubmit"]